import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Categories from '../components/Categories'
import { api } from '../services/ApiService'

export default function Category() {
    const [data,setData] = useState([])
    const deleteCategory = (id) => {
        const afterDelete = data.filter(items => items._id != id)
        setData(afterDelete)
        console.log(id);
        api.delete('category/' + id)
        .then(res => console.log(res.data.message))
      }
    useEffect(() => {
        api.get('category')
        .then(res => {setData(res.data.payload)})
    },[])
  return (
    <div>
        <br/>
        <Container>
        <div>
        <h1 style={{display: 'inline'}}>All Categories</h1>
        <Button variant="info" style={{float: 'right'}} as={Link} to='/addCategory'>New Category</Button>
        </div>
        <br/>
            <Row>
                {
                    data.map((item,index) => (
                        <Col key={index} md={3}>
                        <Categories item={item} deleteCategory={deleteCategory}/>
                        </Col>
                    ))
                }
            </Row>
        </Container>
    </div>
  )
}

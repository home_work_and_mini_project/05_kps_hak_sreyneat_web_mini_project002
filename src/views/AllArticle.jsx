import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Articles from '../components/Articles'
import { api } from '../services/ApiService'

export default function AllArticle() {
    const [data,setData] = useState([])
    const deleteCategory = (id) => {
        const afterDelete = data.filter(items => items._id != id)
        setData(afterDelete)
        console.log(id);
        api.delete('articles/' + id)
        .then(res => console.log(res.data.message))
      }
    useEffect(() => {
        axios.get('https://ams.hrd-edu.info/api/articles')
        .then(res => setData(res.data.payload))
    },[])
  return (
    <div>
        <Container>
        <div>
        <br/>
        <h1 style={{display: 'inline'}}>All Articles</h1>
        <Button variant="info" style={{float: 'right'}} as={Link} to='/addArticle'>New Article</Button>
        </div>
            <Row>
                {
                    data.map((item,index) => (
                        <Col key={index} md={2}>
                            <Articles item={item} deleteCategory={deleteCategory}/>
                        </Col>
                    ))
                }
            </Row>
        </Container>
    </div>
  )
}

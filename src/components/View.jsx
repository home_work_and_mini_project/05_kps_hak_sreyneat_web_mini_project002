import React from 'react'
import { Button, Card, Container } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom';

export default function View() {
    const loc = useLocation();
    const data = loc.state;
    const nav = useNavigate();
    return (
            <Container className='w-30'>
                <br/>
                <Button variant='info' onClick={() => nav('/category')}>Back</Button>
                <h1>Category name: {data.name}</h1>
            </Container>
            
        
    )
}

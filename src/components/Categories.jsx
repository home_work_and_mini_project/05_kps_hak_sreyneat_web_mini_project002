import React from 'react'
import { Button, Card } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function Categories({ item, deleteCategory }) {
    const nav = useNavigate()
    const viewCategory = () => {
        nav('/viewCategory', { state: { ...item } })
    }
    const onUpdate = (name, id) => {
        nav("/editCategory", { state: { name, id} });
      };
    return (
        <div>
            <Card style={{ width: '12rem', marginTop: '10px' }}>
                <Card.Body>
                    <Card.Title style={{ textAlign: 'center' }}>{item.name}</Card.Title>
                    <Button variant="primary" className="w-100 mb-2" onClick={() => onUpdate(item.name, item._id)}>Edit</Button> <br />
                    <Button variant="warning" className="w-100 mb-2" onClick={viewCategory}>View</Button> <br />
                    <Button variant="danger" className="w-100" onClick={() => deleteCategory(item._id)}>Delete</Button>
                </Card.Body>
            </Card>
        </div>
    )
}

import React from 'react'
import { Button, Card } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function Articles({ item, deleteCategory }) {
    const nav = useNavigate()
    const viewArticles = () => {
        nav('/viewCategory', { state: { ...item } })
    }
    const onUpdate = () => {
        nav("/editArticle", { state: { ...item } });
      };
      const viewArticle = () => {
        nav('/viewArticle',{state: {...item}})
    }
    return (
        <div>
            <Card style={{ width: '12rem', marginTop: '25px' }}>
                <Card.Img variant="top" src={item.image ?? 'https://www.pngkey.com/png/detail/233-2332677_image-500580-placeholder-transparent.png'} style={{ height: '25vh' }} />
                <Card.Body>
                    <Card.Title style={{ height: '5vh' }}>{item.title}</Card.Title>
                    <Card.Text style={{ height: '8vh' }}>
                        {item.description}
                    </Card.Text>
                    <Button variant="primary" className='w-100 mb-2' onClick={() => onUpdate(item._id)}>Edit</Button> <br />
                    <Button variant="warning" className='w-100 mb-2' onClick={viewArticle}>View</Button> <br />
                    <Button variant="danger" className='w-100 ' onClick={() => deleteCategory(item._id)}>Delete</Button>
                </Card.Body>
            </Card>
        </div>
    )
}

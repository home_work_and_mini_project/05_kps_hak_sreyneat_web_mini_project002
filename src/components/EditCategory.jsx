import { Button } from 'react-bootstrap';
import React, { useEffect, useState } from 'react'
import { Container, Form } from 'react-bootstrap';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { api } from '../services/ApiService';

export default function EditCategory() {
    const [name, setName] = useState("");
    const [id, setId] = useState();
    const navigate = useNavigate();

    const handleSubmit = () => {
        api
            .put("/category/" + id, { name })
            .then((res) => console.log(res?.data?.message));
    };
    const beforSubmit = () => {
        handleSubmit();
        navigate("/category");
    };

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const location = useLocation();

    useEffect(() => {
        setName(location.state?.name);
        setId(location.state?.id);
    }, [location]);
    return (
        <div>
            <Container className="w-50">
                <h1>Add New Category</h1>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Title</Form.Label>
                        <Form.Control
                            value={name}
                            type="text"
                            placeholder="name"
                            onChange={handleNameChange}
                        />
                    </Form.Group>
                    <Button variant="danger mx-2" as={Link} to="/category">
                        Canncel
                    </Button>
                    <Button variant="primary" onClick={beforSubmit}>
                        Submit
                    </Button>
                </Form>
            </Container>
        </div>
    );
}

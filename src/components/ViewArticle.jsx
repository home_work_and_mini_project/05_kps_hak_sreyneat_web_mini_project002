import React from 'react'
import { Button, Col, Container, Image, Row } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom';

export default function ViewArticle() {

    const loc = useLocation();
    const data = loc.state;
    const nav = useNavigate();
  return (
    <div className='container'>
      <br/>
      <h1>View Article</h1>
      <Container style={{backgroundColor:'#cedbd3',padding:'15px', height:'75vh',borderRadius:'25px',marginTop:'25px'}}>
        <Button style={{marginTop:'10px'}} variant='warning' onClick={() => nav('/')}>Back</Button>
        <Row style={{marginTop:'25px'}}>
          <Col lg={6}>
            <Image src={data.image} style={{height:"60vh",width:'80vh',borderRadius:'20px'}}/>
          </Col>
          <Col lg={6}>
            <h4>Title: {data.title}</h4>
            <h6>Description: {data.description}</h6>
            <h6>Published: {data.published ? 'True' : 'False'}</h6>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

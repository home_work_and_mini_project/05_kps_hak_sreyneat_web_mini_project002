import { Button, Form } from 'react-bootstrap'
import React, { useState } from 'react'
import { Container } from 'react-bootstrap'
import { api } from '../services/ApiService'
import { useNavigate } from 'react-router-dom'

export default function AddNew() {
    const[name, setName]= useState("")
    const nav=useNavigate()
    const handleNameChange=(e)=>{
        console.log(e.target.value);
        setName(e.target.value)
    }
    const handleSave=()=>{
        api.post('category', {name})
        .then((res) => console.log(res.data.message))
        nav("/category")
    }
    return (
        <Container className='w-50'>
            <h3>Add new Category</h3>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>name</Form.Label>
                    <Form.Control type="text" placeholder="Enter name" onChange={handleNameChange}/>
                </Form.Group>
                <Button variant="primary" onClick={handleSave}>
                    Save
                </Button>
            </Form>
        </Container>
    )
}

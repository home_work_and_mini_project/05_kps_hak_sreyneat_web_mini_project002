import React, { useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { api } from '../services/ApiService'
import img from '../image/default.jpg'
import { useNavigate } from 'react-router-dom'

export default function AddArticle() {
    const[title, setTitle] = useState("")
    const[description, setDescription]= useState("")
    const[published, setPublished]= useState(true)
    const[image, setImage]= useState("")
    const[imageUrl, setImageUrl]= useState()
    const nav= useNavigate();

    const handleTitleChange=(title)=>{
        setTitle(title.target.value)
    }
    const handleDescriptionChange=(des)=>{
        setDescription(des.target.value)
    }
    const handleIsPublishedChange=(isPublished)=>{
        setPublished(isPublished.target.checked)
    }
    const handleImageChange=(img)=>{
        console.log(URL.createObjectURL(img.target.files[0]));
        setImageUrl(URL.createObjectURL(img.target.files[0]))
        const formData = new FormData()
        formData.append('image', img.target.files[0])
        console.log("formData: ", formData.get('image'));
        api.post('images', formData)
        .then(res=> setImage(res.data.payload.url))
    }
    
    const handleSubmit=()=>{
        api.post("articles", {title, description, published, image})
        .then(res=> console.log(res.data.message))
        nav('/')
    }
    return (
        <Container className="w-50">
            <h3>Add new article</h3>
            <Form>
                <Form.Group className="mb-3" >
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" placeholder="Enter Title" onChange={handleTitleChange} />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="description" placeholder="Enter Description" onChange={handleDescriptionChange}/>
                </Form.Group>
                <Form.Group controlId="formFile" className="mb-3">
                    <Form.Label>image file</Form.Label>
                    <Form.Control type="file" onChange={handleImageChange}/>
                </Form.Group>
                <img className="border border-warning" src={imageUrl ?? 'https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg'} alt='preview' style={{height:'200px'}}/>
                <Form.Group className="mb-3" controlId="formBasicCheckbox" >
                    <Form.Check type="checkbox" label="published" onChange={handleIsPublishedChange}/>
                </Form.Group>
                <Button variant="danger"  style={{margin: '10px'}} onClick={() => nav('/')}>Cancel</Button>
                <Button variant="primary" onClick={handleSubmit}>Submit</Button>
            </Form>
        </Container>
    )
}

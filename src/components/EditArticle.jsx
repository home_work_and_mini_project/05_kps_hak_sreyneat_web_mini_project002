import { Button } from 'react-bootstrap';
import React, { useEffect, useState } from 'react'
import { Container, Form } from 'react-bootstrap';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { api } from '../services/ApiService';

export default function EditArticle() {
  const [id, setId] = useState();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [published, setPublished] = useState(true);
  const [image, setImage] = useState("");
  const [imageUrl, setImageUrl] = useState();
  const [category, setCategory] = useState("");

  const navigate = useNavigate();

  const beforSubmit = () => {
    
        handleSubmit();
        navigate("/");
     
    };


    const handleSubmit = () => {
      api
        .patch("/articles/" + id, {
          title,
          description,
          published,
          image,
          category,
        })
        .then((res) => console.log(res?.data?.message));
    };

    const handleTitleChange = (e) => {
      setTitle(e.target.value);
    };
  
    useEffect(() => {
      console.log(title);
    }, [title]);
  
    const handleDesChange = (e) => {
      setDescription(e.target.value);
    };
  
    const publishedChange = (e) => {
      setPublished(e.target.checked);
    };
  
    const handleImageChange = (e) => {
      console.log("e.target.files[0] " + e.target.files[0]);
      console.log(URL.createObjectURL(e.target.files[0]));
      setImageUrl(URL.createObjectURL(e.target.files[0]));
      const formData = new FormData();
      formData.append("image", e.target.files[0]);
      console.log("FormData : ", formData.get("image"));
      api
        .post("/images", formData)
        .then((res) => setImage(res?.data?.payload?.url));
    };
  
    const location = useLocation();
    const data = location.state;
    useEffect(() => {
      setId(data._id);
      setTitle(data.title);
      setDescription(data.description);
      setImage(
        data.image ??
          "https://www.pulsecarshalton.co.uk/wp-content/uploads/2016/08/jk-placeholder-image.jpg",
      );
      setCategory(data.category);
      setPublished(data.isPublished);
    }, [location]);
  
  
  
  return (
    <div className='container'>
      <h1>Edit Article</h1>
      <Container className="w-50">
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            value={title}
            placeholder="title"
            onChange={handleTitleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            value={description}
            placeholder="description"
            onChange={handleDesChange}
          />
        </Form.Group>
        <Form.Group controlId="formFile" className="mb-3">
          <Form.Label>Image File</Form.Label>
          <Form.Control type="file" onChange={handleImageChange} />
        </Form.Group>
        <img
          src={imageUrl ?? image}
          alt="preview"
          style={{ height: "200px" }}
        />
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check
            type="checkbox"
            checked={published}
            label="Is Published"
            onChange={publishedChange}
          />
        </Form.Group>
        <Button variant="danger mx-2" as={Link} to="/">
          Canncel
        </Button>
        <Button variant="primary" onClick={beforSubmit}>
          Submit
        </Button>
      </Form>
    </Container>
   
    </div>
  )
}

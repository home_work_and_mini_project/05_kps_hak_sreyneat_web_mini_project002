import logo from './logo.svg';
import './App.css';
import AllArticle from './views/AllArticle';
import Category from './views/Category';
import NavBarComponent from './components/NavBarComponent';
import {Route, Routes } from 'react-router-dom';
import AddArticle from './components/AddArticle';
import AddNew from './components/AddNew';
import View from './components/View';
import EditCategory from './components/EditCategory';
import ViewArticle from './components/ViewArticle';
import EditArticle from './components/EditArticle';

function App() {
  return (
    <div className="App">
      <NavBarComponent/>
      <Routes>
        <Route path="/" to element={<AllArticle/>}/>
        <Route path="/category" to element={<Category/>}/>
        <Route path="addCategory" to element={<AddNew/>}/>
        <Route path="addArticle" to element={<AddArticle/>}/>
        <Route path='viewCategory' to element={<View/>}/>
        <Route path='editCategory' to element={<EditCategory/>}/>
        <Route path='editArticle' to element={<EditArticle/>}/>
        <Route path='viewArticle' to element={<ViewArticle/>}/>
      </Routes>
    </div>
  );
}

export default App;
